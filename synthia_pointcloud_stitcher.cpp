//ROS includes
#include <rosbag/view.h>
#include <pcl_ros/transforms.h>
#include <sensor_msgs/point_cloud2_iterator.h>

//PCL includes
#include <pcl/io/ply_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/PCLPointCloud2.h>

//Boost includes
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>

//Definitions
#define CUTOFF_RADIUS 30    //meters
#define foreach BOOST_FOREACH
#define PI 3.141592653589793238462643383279f

//Convenience namespaces
using namespace std;
using namespace boost::filesystem;
using namespace pcl;

//Rotation convenience and readability functions
class Rotation {
public:
    static Eigen::Matrix4f getRotationAroundX(float angle) {
        Eigen::Matrix4f rotation;
        rotation << 1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, cos(angle * PI / 180), sin(angle * PI / 180), 0.0f,
                0.0f, -sin(angle * PI / 180), cos(angle * PI / 180), 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
        return rotation;
    }

    static Eigen::Matrix4f getRotationAroundY(float angle) {
        Eigen::Matrix4f rotation;
        rotation << cos(angle * PI / 180), 0.0f, -sin(angle * PI / 180), 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                sin(angle * PI / 180), 0.0f, cos(angle * PI / 180), 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
        return rotation;
    }

    static Eigen::Matrix4f getRotationAroundZ(float angle) {
        Eigen::Matrix4f rotation;
        rotation << cos(angle * PI / 180), sin(angle * PI / 180), 0.0f, 0.0f,
                -sin(angle * PI / 180), cos(angle * PI / 180), 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
        return rotation;
    }
    static Eigen::Matrix4f getTransformationFromMsgStamped(geometry_msgs::TransformStamped &input){
        //Conversion from ros TransformStamped message to 4x4 Transformation matrix
        tf::Quaternion quat;
        tf::quaternionMsgToTF(input.transform.rotation, quat);
        tf::Matrix3x3 rotation(quat);

        tf::Vector3 translation;
        tf::vector3MsgToTF(input.transform.translation, translation);

        Eigen::Matrix4f transformation;
        transformation << rotation.getRow(0)[0] , rotation.getRow(0)[1] , rotation.getRow(0)[2] , translation[0],
                          rotation.getRow(1)[0] , rotation.getRow(1)[1] , rotation.getRow(1)[2] , translation[1],
                          rotation.getRow(2)[0] , rotation.getRow(2)[1] , rotation.getRow(2)[2] , translation[2],
                          0.0f , 0.0f , 0.0f , 1.0f;

        return transformation;
    }
};

//Struct to handle standard transformations
struct Transformations {
private:
    float correction_angle;
    public:
    Eigen::Matrix4f cam_to_global_front;
    Eigen::Matrix4f cam_to_global_back;
    Eigen::Matrix4f cam_to_global_left;
    Eigen::Matrix4f cam_to_global_right;

    Eigen::Matrix4f global_to_world;
    Eigen::Matrix4f world_to_global_corrected;

    Transformations() : correction_angle(-83.0f) {

        //Transformations from camera to global frame
        cam_to_global_front = Rotation::getRotationAroundZ(90.0f)*
                              Rotation::getRotationAroundX(90.0f);

        cam_to_global_back  = Rotation::getRotationAroundZ(-90.0f)*
                              Rotation::getRotationAroundX(90.0f);

        cam_to_global_left  = Rotation::getRotationAroundX(90);

        cam_to_global_right = Rotation::getRotationAroundX(90)*
                              Rotation::getRotationAroundY(180);

        global_to_world     = Rotation::getRotationAroundY(-90.0)*
                              Rotation::getRotationAroundX(90.0f);

        //Correction to put origin into ground plane and correct weird downfacing camera angle
        Eigen::Matrix4f correction_translation;
        correction_translation << 1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 1.4f,
                0.0f, 0.0f, 0.0f, 1.0f;

        world_to_global_corrected = correction_translation*
                     Rotation::getRotationAroundZ(90.0)*
                     Rotation::getRotationAroundX(correction_angle);
    }
};

//Filter out weird zero entries from rosbag
void filterZeros(PointCloud<PointXYZRGBL>::Ptr &input, PointCloud<PointXYZRGBL>::Ptr &output) {
    PassThrough <PointXYZRGBL> pass;
    pass.setFilterFieldName("x");
    pass.setNegative(true);
    pass.setFilterLimits(-0.001f, 0.001f);

    pass.setInputCloud(input);
    pass.filter(*output);
}

//Merge label cloud and rgb cloud by adding a label vertex to rgb cloud
void convertPC2MessageToPCL(sensor_msgs::PointCloud2 &input_cloud_msg, PointCloud<PointXYZRGBL>::Ptr &output_cloud) {

    //create iterators for PC2 message
    sensor_msgs::PointCloud2Iterator<float> xyz(input_cloud_msg, "x");
    sensor_msgs::PointCloud2Iterator<uint8_t> rgb(input_cloud_msg, "rgb");
    sensor_msgs::PointCloud2Iterator<uint8_t> label(input_cloud_msg, "label");

    for (; xyz != xyz.end(); ++xyz, ++rgb, ++label) {
        PointXYZRGBL point;
        //copy regular point stuff
        point.x = xyz[0];
        point.y = xyz[1];
        point.z = xyz[2];
        //PC2 message is BGR
        point.r = (uint8_t)rgb[2];
        point.g = (uint8_t)rgb[1];
        point.b = (uint8_t)rgb[0];
        //copy label
        point.label = label[0];

        output_cloud->push_back(point);
    }
}

//Conversion from absolute transforms to transforms between pointclouds
void convertTransforms (vector<geometry_msgs::TransformStamped> &input_tfs, vector<Eigen::Matrix4f> &output_tfs){
    Transformations transformations;

    //Apply transformation in 'world' frame of imu
    for (int index = 0; index < input_tfs.size(); index++ ){
        //Transform pointcloud by imu pose in world frame
        //Pointclouds are in global frame (this frame is also the desired output frame)
        //Transformations are in a (ros) world frame
        //Therefore we need to: 1) go from global to world frame, 2) apply transformation in world frame
        // 3) backtransform everything with a frame from the middle of the transformation set (so that coordinate system
        //is centered later) and 4) rotate back into world frame
        //
        //one could also calculate the difference transformation A->B as A->Origin->B where A->Origin is inverse(A)
        //in the end it's the same - Note that transformations are not real transformations of the entire pointcloud
        //but 'simply' 4x4 Matrix multiplications
        Eigen::Matrix4f n_to_last;
        n_to_last = transformations.world_to_global_corrected*
                    Rotation::getTransformationFromMsgStamped(input_tfs[input_tfs.size()/2]).inverse()*
                    Rotation::getTransformationFromMsgStamped(input_tfs[index])*
                    transformations.global_to_world;

        output_tfs.push_back(n_to_last);
    }
}

//Apply the actual transformation
void transformRosbagToPCL(rosbag::View &view, std::string &output_dir, int &batch_size){
    //Prepare neccessary directories
    if (!is_directory(path(output_dir + "/raw")))
        create_directory(path(output_dir + "/raw"));
    if (!is_directory(path(output_dir + "/stitched")))
        create_directory(path(output_dir + "/stitched"));

    //Initialize pointcloud transformation stuff
    Transformations transformations;

    //initialize loop variables/containers
    //some counters
    int message_count = 0;
    int cloud_count = 0;
    int batch_count = 0;

    //container for piecewise collection of clouds
    PointCloud<PointXYZRGBL>::Ptr global_cloud(new PointCloud <PointXYZRGBL>);

    //container for collection of full 360 degree clouds
    vector<PointCloud<PointXYZRGBL>::Ptr> batch_global_cloud;

    //container for transformations
    vector<geometry_msgs::TransformStamped> transforms;

    //loop over rosbag
    foreach(rosbag::MessageInstance const m, view)
    {
        //Parse the topic name string
        string topic = m.getTopic();

        if (topic.substr(0, 11) == "Stereo_Left"){
            cloud_count++;
            cout << "Received cloud: " << cloud_count << endl;

            //Parse message content
            PointCloud<PointXYZRGBL>::Ptr pcl_temp(new PointCloud <PointXYZRGBL>);
            convertPC2MessageToPCL(*m.instantiate<sensor_msgs::PointCloud2>(), pcl_temp);

            //Apply transformation
            if(topic.substr(17, 1) == "B")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_global_back);
            if(topic.substr(17, 1) == "F")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_global_front);
            if(topic.substr(17, 1) == "L")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_global_left);
            if(topic.substr(17, 1) == "R")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_global_right);

                *global_cloud += *pcl_temp;
        }

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //4 clouds (that is one full 360 degrees set) have been loaded
        if (cloud_count == 4) {
            cloud_count = 0;
            message_count++;

            cout << "Processing message: " << message_count << endl;

            //weird zero entries are present in original rosbag
            filterZeros(global_cloud, global_cloud);

            //Apply radius filter
            PointIndices::Ptr inliers(new PointIndices());
            for (int i=0; i < global_cloud->size(); i++){
                if ((pow(global_cloud->points[i].x, 2) + pow(global_cloud->points[i].y,2)) <= pow(CUTOFF_RADIUS,2) ){
                    inliers->indices.push_back(i);
                }
            }

            ExtractIndices<PointXYZRGBL> extract;
            extract.setInputCloud(global_cloud);
            extract.setNegative(false);
            extract.setIndices(inliers);
            extract.filter(*global_cloud);

            //Apply Voxelgrid filter
            VoxelGrid<PointXYZRGBL> voxfilter;

            voxfilter.setLeafSize (0.05f, 0.05f, 0.05f);
            voxfilter.setInputCloud(global_cloud);
            voxfilter.filter(*global_cloud);

            //Copy data into new container
            PointCloud<PointXYZRGBL>::Ptr pushback_cloud(new PointCloud <PointXYZRGBL>);
            *pushback_cloud = *global_cloud;

            //push piecewise collected clouds into batch vector (remark: only pointers are pushed, hence copy data above)
            batch_global_cloud.push_back(pushback_cloud);

            //Free up the containers
            global_cloud->clear();
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // one batch of pointclouds has been stored temporarily - make sure all transformations are stored aswell
        if ((message_count%batch_size) == 0 && message_count != 0 && cloud_count == 0 && transforms.size() == batch_size){
            batch_count++;

            stringstream ss;
            ss << setw(6) << setfill('0') << batch_count;
            string batch_index = ss.str();

            //get the necessary transforms for stitching
            vector<Eigen::Matrix4f> _transforms;
            convertTransforms(transforms, _transforms); //From tf to matrix4f

            cout << "Generating pointcloud " << batch_index << endl;

            //Stitch pointclouds together using transformations from imu pose
            PointCloud<PointXYZRGBL>::Ptr rgb_final_cloud(new PointCloud <PointXYZRGBL>);

            for (int index = 0; index < batch_size ; index++) {

                //For some reason only bitshift works for accessing a eigen matrix in a vector
                //direct access gives undefined behaviour (random segmentation faults)
                Eigen::Matrix4f transformation;
                transformation << _transforms[index];

                transformPointCloud(*batch_global_cloud[index], *batch_global_cloud[index],
                                         transformation);

                *rgb_final_cloud += *batch_global_cloud[index];

                stringstream stream;
                stream << setw(6) << setfill('0') << (batch_count-1)*batch_size+index;
                string cloud_index = stream.str();

                //Uncomment for output of single RGB/Label clouds
                io::savePLYFileASCII(output_dir + "raw/" + cloud_index + ".ply", *batch_global_cloud[index]);
            }
            //Even when composed of voxelgrid filtered pointclouds, pointclouds tend to be to big for larger batch sizes
            //filter all pointclouds > 2mio. points again using voxelgrid filter
            if (rgb_final_cloud->width > 2000000) {
                VoxelGrid <PointXYZRGBL> voxfilter;
                voxfilter.setLeafSize(0.05f, 0.05f, 0.05f);

                voxfilter.setInputCloud(rgb_final_cloud);
                voxfilter.filter(*rgb_final_cloud);
            }
            //save pointclouds
            io::savePLYFileASCII(output_dir + "stitched/" + batch_index + ".ply", *rgb_final_cloud);

            //clear loop containers
            transforms.clear();
            batch_global_cloud.clear();
        }

        if (m.getTopic() == "transform_imu") {
            cout << "Received transform message at: " << cloud_count << " pointcloud messages" << endl;
            transforms.push_back(*m.instantiate<geometry_msgs::TransformStamped>());
        }
    }
}

int main(int argc, char **argv) {
    //Parse Arguments
    if (argc < 3) {
        ROS_ERROR("Please specify at least: 1) input bag file and: 2) output directory. Optionally you can provide: 3) batch size (default = 10).");
        return -1;
    }
    string output_dir = argv[2];
    if (!is_directory(output_dir)) {
        ROS_ERROR("Specified output directory does not exist - aborting...");
        return -1;
    }
    if (output_dir.substr((output_dir.size() - 1)) != "/")
        output_dir += "/";

    int batch_size = 10; //default
    if (argc > 3) {
        batch_size = atoi(argv[3]);
        if (batch_size < 1) {
            ROS_ERROR("Invalid batch size argument - terminating...");
            return -1;
        }
    }

    //Initialize ROS node
    ros::init(argc, argv, "rosbag_to_neural_net");
    ros::NodeHandle n;

    //Prepare Rosbag
    string bag_file = argv[1];
    //Read the rosbag
    cout << "Reading Rosbag..." << endl;

    rosbag::Bag bag;
    bag.open(bag_file, rosbag::bagmode::Read);

    //Specify topics to query
    std::vector <std::string> topics;
    topics.push_back(std::string("Stereo_Left/Omni_B/rgb_cloud"));
    topics.push_back(std::string("Stereo_Left/Omni_F/rgb_cloud"));
    topics.push_back(std::string("Stereo_Left/Omni_L/rgb_cloud"));
    topics.push_back(std::string("Stereo_Left/Omni_R/rgb_cloud"));
    topics.push_back(std::string("transform_imu"));

    rosbag::View view(bag, rosbag::TopicQuery(topics));

    //Do the actual transformation (main part)
    transformRosbagToPCL(view, output_dir, batch_size);

    bag.close();
    return 0;
}