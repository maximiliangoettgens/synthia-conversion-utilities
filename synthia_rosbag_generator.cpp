//ROS includes.
#include <rosbag/view.h>
#include <pcl_ros/transforms.h>

//PCL includes.
#include <pcl/io/ply_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <geometry_msgs/Pose.h>
//Boost includes.
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>

//Definitions.
#define CUTOFF_RADIUS 30        //meters
#define CUTOFF_HEIGHT 4.0f      //meters
#define VOXELGRID_LEAFSIZE 0.05f//meters
#define foreach BOOST_FOREACH
#define PI 3.141592653589793238462643383279f

//Convenience namespaces.
using namespace std;
using namespace boost::filesystem;
using namespace pcl;
typedef PointCloud<PointXYZRGBL> pointcloud;
using namespace Eigen;

//Rotation convenience and readability functions.
class Rotation {
public:
    static Matrix4f getRotationAroundX(float angle) {
        Matrix4f rotation;
        rotation << 1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, cos(angle * PI / 180), sin(angle * PI / 180), 0.0f,
                0.0f, -sin(angle * PI / 180), cos(angle * PI / 180), 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
        return rotation;
    }

    static Matrix4f getRotationAroundY(float angle) {
        Matrix4f rotation;
        rotation << cos(angle * PI / 180), 0.0f, -sin(angle * PI / 180), 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                sin(angle * PI / 180), 0.0f, cos(angle * PI / 180), 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
        return rotation;
    }

    static Matrix4f getRotationAroundZ(float angle) {
        Matrix4f rotation;
        rotation << cos(angle * PI / 180), sin(angle * PI / 180), 0.0f, 0.0f,
                -sin(angle * PI / 180), cos(angle * PI / 180), 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
        return rotation;
    }
    static Matrix4f getTransformationFromMsgStamped(geometry_msgs::TransformStamped &input){
        //Conversion from ros TransformStamped message to 4x4 Transformation matrix.
        tf::Quaternion quat;
        tf::quaternionMsgToTF(input.transform.rotation, quat);
        tf::Matrix3x3 rotation(quat);

        tf::Vector3 translation;
        tf::vector3MsgToTF(input.transform.translation, translation);

        Matrix4f transformation;
        transformation << rotation.getRow(0)[0] , rotation.getRow(0)[1] , rotation.getRow(0)[2] , translation[0],
                rotation.getRow(1)[0] , rotation.getRow(1)[1] , rotation.getRow(1)[2] , translation[1],
                rotation.getRow(2)[0] , rotation.getRow(2)[1] , rotation.getRow(2)[2] , translation[2],
                0.0f , 0.0f , 0.0f , 1.0f;

        return transformation;
    }
};

//Struct to handle standard transformations.
struct Transformations {
private:
    float correction_angle;
public:
    Matrix4f cam_to_scene_front;
    Matrix4f cam_to_scene_back;
    Matrix4f cam_to_scene_left;
    Matrix4f cam_to_scene_right;

    Matrix4f scene_to_world;
    Matrix4f world_to_scene_corrected;
    Matrix4f scene_to_filter;

    Transformations() : correction_angle(-83.0f) {

        //Transformations from camera to scene frame.
        cam_to_scene_front = Rotation::getRotationAroundZ(90.0f)*
                             Rotation::getRotationAroundX(90.0f);

        cam_to_scene_back  = Rotation::getRotationAroundZ(-90.0f)*
                             Rotation::getRotationAroundX(90.0f);

        cam_to_scene_left  = Rotation::getRotationAroundX(90);

        cam_to_scene_right = Rotation::getRotationAroundX(90)*
                             Rotation::getRotationAroundY(180);

        scene_to_world     = Rotation::getRotationAroundY(-90.0)*
                             Rotation::getRotationAroundX(90.0f);

        //Correction to put origin into ground plane and correct weird downfacing camera angle.
        Matrix4f correction_translation;
        correction_translation << 1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 1.4f,
                0.0f, 0.0f, 0.0f, 1.0f;

        world_to_scene_corrected = correction_translation*
                                   Rotation::getRotationAroundZ(90.0)*
                                   Rotation::getRotationAroundX(correction_angle);

        scene_to_filter = correction_translation*
                          Rotation::getRotationAroundY(-7.0);
    }
};

class Conversion {
public:
    //Conversion from ros PC2 to ros PCL.
    static void convertPC2MessageToPCL(sensor_msgs::PointCloud2 &input_cloud_msg, pointcloud::Ptr &output_cloud) {

        //Create iterators for PC2 message
        sensor_msgs::PointCloud2Iterator<float> xyz(input_cloud_msg, "x");
        sensor_msgs::PointCloud2Iterator<uint8_t> rgb(input_cloud_msg, "rgb");
        sensor_msgs::PointCloud2Iterator<uint8_t> label(input_cloud_msg, "label");

        for (; xyz != xyz.end(); ++xyz, ++rgb, ++label) {
            PointXYZRGBL point;
            //Copy regular point stuff.
            point.x = xyz[0];
            point.y = xyz[1];
            point.z = xyz[2];
            //PC2 message is BGR.
            point.r = (uint8_t)rgb[2];
            point.g = (uint8_t)rgb[1];
            point.b = (uint8_t)rgb[0];
            //Copy label.
            point.label = label[0];

            output_cloud->push_back(point);
        }
    }

    //Conversion from absolute transforms to transforms between pointclouds.
    static void convertTransforms (vector<geometry_msgs::TransformStamped> &input_tfs, vector<Matrix4f> &output_tfs){
        Transformations transformations;

        //Apply transformation in 'world' frame of imu
        for (int index = 0; index < input_tfs.size(); index++ ){
            //Transform pointcloud by imu pose in world frame.
            //Pointclouds are in scene frame (this frame is also the desired output. frame)
            //Transformations are in a (ros) world frame.
            //Therefore we need to: 1) go from scene to world frame, 2) apply transformation in world frame
            // 3) backtransform everything with a frame from the middle of the transformation set (so that coordinate system
            //is centered later) and 4) rotate back into world frame.
            //
            //One could also calculate the difference transformation A->B as A->Origin->B where A->Origin is inverse(A)
            //in the end it's the same - Note that transformations are not real transformations of the entire pointcloud
            //but 'simply' 4x4 Matrix multiplications.
            Matrix4f n_to_last;
            n_to_last = transformations.world_to_scene_corrected*
                        Rotation::getTransformationFromMsgStamped(input_tfs[input_tfs.size()/2]).inverse()*
                        Rotation::getTransformationFromMsgStamped(input_tfs[index])*
                        transformations.scene_to_world;

            output_tfs.push_back(n_to_last);
        }
    }

    //Conversion from PCL to ros PC2.
    static void convertPCLToPC2Message(pointcloud::Ptr &input_cloud, sensor_msgs::PointCloud2 &output_cloud){
        //Reset fields and reserve 7 fields = XYZRGBL.
        output_cloud.fields.clear();
        output_cloud.fields.reserve(7);

        //Set fields.
        int offset = 0;
        offset = addPointField(output_cloud, "x", 1, sensor_msgs::PointField::FLOAT32, offset);
        offset = addPointField(output_cloud, "y", 1, sensor_msgs::PointField::FLOAT32, offset);
        offset = addPointField(output_cloud, "z", 1, sensor_msgs::PointField::FLOAT32, offset);
        offset = addPointField(output_cloud, "rgb", 1, sensor_msgs::PointField::FLOAT32, offset);
        offset = addPointField(output_cloud, "label", 1, sensor_msgs::PointField::UINT8, offset);

        //Resize Pointcloud properly.
        output_cloud.height = 1;
        output_cloud.width = input_cloud->size();
        output_cloud.point_step = offset;
        output_cloud.row_step = output_cloud.width * output_cloud.point_step;
        output_cloud.data.resize(output_cloud.width * output_cloud.point_step);

        sensor_msgs::PointCloud2Modifier pcd_modifier(output_cloud);

        //Copy data.
        sensor_msgs::PointCloud2Iterator<float> iter_x(output_cloud, "x");
        sensor_msgs::PointCloud2Iterator<float> iter_y(output_cloud, "y");
        sensor_msgs::PointCloud2Iterator<float> iter_z(output_cloud, "z");
        sensor_msgs::PointCloud2Iterator<uint32_t> iter_rgb(output_cloud, "rgb");
        sensor_msgs::PointCloud2Iterator<uint8_t> iter_label(output_cloud, "label");

        for (int i = 0; i < input_cloud->points.size(); ++i, ++iter_x, ++iter_y,
                ++iter_z, ++iter_rgb, ++iter_label) {
            *iter_x = input_cloud->points[i].x;
            *iter_y = input_cloud->points[i].y;
            *iter_z = input_cloud->points[i].z;

            *iter_rgb = (uint32_t)((uint8_t) input_cloud->points[i].b) |
                        (uint32_t)((uint8_t) input_cloud->points[i].g << 8) |
                        (uint32_t)((uint8_t) input_cloud->points[i].r << 16);

          //Convert labels
          int input_label = input_cloud->points[i].label;
          //Void, Sky, 2x'Reserved', Pedestrian, Bicycle -> Clutter
          if (//points_it->label == 0 ||
              input_label == 1 ||
              input_label == 10 ||
              input_label == 11 ||
              input_label == 13 ||
              input_label == 14){
            *iter_label = (uint8_t) 0;
            continue;
          }
          //Road, Sidewalk, Lanemarking -> Groundplane
          if (input_label == 3 ||
              input_label == 4 ||
              input_label == 12){
            *iter_label = (uint8_t) 1;
            continue;
          }
          //Vegetation -> Vegetation
          if (input_label == 6 ){
            *iter_label = (uint8_t) 2;
            continue;
          }
          //Building -> Building
          if (input_label == 2 ){
            *iter_label = (uint8_t) 3;
            continue;
          }
          //Most of Void, Pole, Traffic Sign, Traffic Light, Fence -> Man-made structure
          if (input_label == 0 ||
              input_label == 5 ||
              input_label == 7 ||
              input_label == 9 ||
              input_label == 15){
            *iter_label = (uint8_t) 4;
            continue;
          }
          //Car -> Car
          if (input_label == 8){
            *iter_label = (uint8_t) 5;
            continue;
          }
          *iter_label = (uint8_t) 0;   //default - should never be called though
        }
    }
};

//Filter out weird zero entries from rosbag.
void filterZeros(pointcloud::Ptr &input, pointcloud::Ptr &output) {
    PassThrough <PointXYZRGBL> pass;
    pass.setFilterFieldName("x");
    pass.setNegative(true);
    pass.setFilterLimits(-0.001f, 0.001f);

    pass.setInputCloud(input);
    pass.filter(*output);
}

//Cut Pointclouds at certain height to simulate capture by sensor modality.
void cutAtHeight(pointcloud::Ptr &input, pointcloud::Ptr &output, float height){
    //Apply filter at certain height to simulate sensor modality.
    //Apply filter at -5m for clearance below groundplane.
    pcl::PassThrough<pcl::PointXYZRGBL> pass;
    pass.setFilterFieldName("z");
    pass.setFilterLimits(-5.0f, height);

    pass.setInputCloud(input);
    pass.filter(*output);
}

void applyVoxelgridFilter(pointcloud::Ptr &input, float size){
    //Voxelgrid filter suffers from index overflow when dealing with to large pointclouds
    //or to small leaf size. Hence, pointcloud is separated into subclouds of size 100VOXELGRID_LEAFSIZE*100VOXELGRID_LEAFSIZE.

    //Determine Pointcloud extent.
    float metavoxel_size = 1000*size;
    Array4f min_p, max_p;
    min_p.setConstant(FLT_MAX);
    max_p.setConstant(-FLT_MAX);

    for (size_t i = 0; i < input->points.size(); ++i) {
        Array4f pt;
        pt << input->points[i].x, input->points[i].y, input->points[i].z, 0.0f;
        min_p = min_p.min (pt);
        max_p = max_p.max (pt);
    }

    pointcloud::Ptr filtered_cloud(new PointCloud <PointXYZRGBL>);
    pointcloud::Ptr tempCloud(new PointCloud <PointXYZRGBL>);

    //Separate Pointcloud into Subpointclouds that can be Voxelgrid filtered.
    for (float x = min_p[0]; x < max_p[0]; x += metavoxel_size){
        for (float y = min_p[0]; y < max_p[1]; y += metavoxel_size) {
            //Extract subcloud patch by filtering x and y.
            //X.
            PassThrough<PointXYZRGBL> x_pass;
            x_pass.setFilterFieldName("x");
            x_pass.setFilterLimits(x,x + metavoxel_size);

            x_pass.setInputCloud(input);
            x_pass.filter(*tempCloud);

            //Y.
            PassThrough<PointXYZRGBL> y_pass;
            y_pass.setFilterFieldName("y");
            y_pass.setFilterLimits(y,y + metavoxel_size);

            y_pass.setInputCloud(tempCloud);
            y_pass.filter(*tempCloud);

            //Apply Voxelgrid filter to smaller subcloud.
            VoxelGrid <PointXYZRGBL> voxfilter;
            voxfilter.setLeafSize(size, size, size);

            voxfilter.setInputCloud(tempCloud);
            voxfilter.filter(*tempCloud);

            *filtered_cloud += *tempCloud;
            tempCloud->clear();
        }
    }
    *input = *filtered_cloud;
}

//Apply the actual transformation
void transformRosbagToPCL(rosbag::View &view, rosbag::Bag &rosbag, int batch_size, bool global_cloud){

    if(global_cloud) //Process all in one.
        batch_size = (view.size() / 5) - 1;

    //Initialize pointcloud transformation stuff.
    Transformations transformations;

    //Initialize loop variables/containers.
    //Some counters.
    float time = 0.0f;
    int message_count = 0;
    int transform_count = 0;
    int cloud_count = 0;
    int batch_count = 0;

    //Container for piecewise collection of clouds.
    pointcloud::Ptr scene_cloud(new PointCloud <PointXYZRGBL>);

    //Container for collection of full 360 degree clouds.
    vector<pointcloud::Ptr> batch_scene_cloud;

    //Container for transformations.
    vector<geometry_msgs::TransformStamped> transforms;

    //Loop over rosbag.
    foreach(rosbag::MessageInstance const m, view)
    {
        //Parse the topic name string.
        string topic = m.getTopic();

        if (topic.substr(0, 11) == "Stereo_Left"){
            cloud_count++;
            cout << "Received cloud: " << cloud_count << endl;

            //Parse message content.
            pointcloud::Ptr pcl_temp(new PointCloud <PointXYZRGBL>);
            sensor_msgs::PointCloud2 bag_cloud = *m.instantiate<sensor_msgs::PointCloud2>();

            Conversion::convertPC2MessageToPCL(bag_cloud, pcl_temp);

            //Apply transformation.
            if(topic.substr(17, 1) == "B")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_scene_back);
            if(topic.substr(17, 1) == "F")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_scene_front);
            if(topic.substr(17, 1) == "L")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_scene_left);
            if(topic.substr(17, 1) == "R")
                transformPointCloud(*pcl_temp, *pcl_temp, transformations.cam_to_scene_right);

            *scene_cloud += *pcl_temp;
        }

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Four clouds (that is one full 360 degrees set) have been loaded.
        if (cloud_count == 4) {
            cloud_count = 0;
            message_count++;

            cout << "Processing scene: " << message_count << endl;

            transformPointCloud(*scene_cloud, *scene_cloud, transformations.scene_to_filter);

            //Weird zero entries are present in original rosbag.
            filterZeros(scene_cloud, scene_cloud);

            //Cut at some height to simulate sensor modality limits.
            cutAtHeight(scene_cloud, scene_cloud, (float)CUTOFF_HEIGHT);

            //Apply radius filter.
            PointIndices::Ptr inliers(new PointIndices());
            for (int i=0; i < scene_cloud->size(); i++){
                if ((pow(scene_cloud->points[i].x, 2) + pow(scene_cloud->points[i].y,2)) <= pow(CUTOFF_RADIUS,2) ){
                    inliers->indices.push_back(i);
                }
            }
            ExtractIndices<PointXYZRGBL> extract;
            extract.setInputCloud(scene_cloud);
            extract.setNegative(false);
            extract.setIndices(inliers);
            extract.filter(*scene_cloud);

            //Preliminary Voxelgrid filter to reduce memory consumption for scene cloud.
            VoxelGrid <PointXYZRGBL> voxfilter;
            voxfilter.setLeafSize(VOXELGRID_LEAFSIZE, VOXELGRID_LEAFSIZE, VOXELGRID_LEAFSIZE);

            voxfilter.setInputCloud(scene_cloud);
            voxfilter.filter(*scene_cloud);

            //Copy data into new container.
            pointcloud::Ptr pushback_cloud(new PointCloud <PointXYZRGBL>);
            transformPointCloud(*scene_cloud, *pushback_cloud, (Matrix4f) transformations.scene_to_filter.inverse());

            //Push piecewise collected clouds into batch vector. (remark: only pointers are pushed, hence copy data above).
            batch_scene_cloud.push_back(pushback_cloud);

            //Free up the containers.
            scene_cloud->clear();
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //One batch of pointclouds has been stored temporarily - make sure all transformations are stored aswell.
        if ((message_count%batch_size) == 0 && message_count != 0 && cloud_count == 0 && transforms.size() == batch_size){
            batch_count++;

            stringstream ss;
            ss << setw(6) << setfill('0') << batch_count;
            string batch_index = ss.str();

            //Get the necessary transforms for stitching.
            vector<Matrix4f> _transforms;
            Conversion::convertTransforms(transforms, _transforms); //From tf to Eigen::Matrix4f.

            if (global_cloud)
                cout << "Generating Global Cloud " << endl;
            else
                cout << "Generating Query Cloud: " << batch_index << endl;


            //Stitch pointclouds together using transformations from imu pose.
            pointcloud::Ptr rgb_final_cloud(new PointCloud <PointXYZRGBL>);

            cout << "Stitching..." << endl;
            for (int index = 0; index < batch_size ; index++) {

                //For some reason only bitshift works for accessing a eigen matrix in a vector
                //direct access gives undefined behaviour. (random segmentation faults)
                Matrix4f transformation;
                transformation << _transforms[index];

                transformPointCloud(*batch_scene_cloud[index], *batch_scene_cloud[index],
                                    transformation);

                *rgb_final_cloud += *batch_scene_cloud[index];
            }

            //Clear loop containers.
            transforms.clear();
            batch_scene_cloud.clear();

            //Final Voxelgrid Filter (on subclouds).
            cout << "Filtering..." << endl;
            applyVoxelgridFilter(rgb_final_cloud, VOXELGRID_LEAFSIZE);
            //Save pointclouds.
            cout << "Extracting Rosbag..." << endl;
            if(global_cloud) {
                sensor_msgs::PointCloud2 _global_cloud;
                Conversion::convertPCLToPC2Message(rgb_final_cloud, _global_cloud);
                _global_cloud.header.frame_id = "world";
                _global_cloud.header.seq = 1;

                ros::Time global_time(0.01f);

                rosbag.write("/global_cloud", global_time, _global_cloud);
            }
            else {
                sensor_msgs::PointCloud2 _query_cloud;
                Conversion::convertPCLToPC2Message(rgb_final_cloud, _query_cloud);
                _query_cloud.header.frame_id = "world";
                _query_cloud.header.seq = batch_count;

                time += batch_size * 0.2f;   //Assuming capturing runs at 5Hz
                ros::Time query_time(time);

                rosbag.write("/query_cloud", query_time, _query_cloud);

                //write transformation
                geometry_msgs::Pose pose_msg;

                pose_msg.position.x = _transforms[batch_size/2](0,3);
                pose_msg.position.y = _transforms[batch_size/2](1,3);
                pose_msg.position.z = _transforms[batch_size/2](2,3);

                pose_msg.orientation.x = 0;
                pose_msg.orientation.y = 0;
                pose_msg.orientation.z = 0;
                pose_msg.orientation.w = 1;

                rosbag.write("/ground_truth_pose", query_time, pose_msg);
            }
        }

        if (m.getTopic() == "transform_imu") {
            transform_count++;
            cout << "Received transform: " << transform_count << endl;
            transforms.push_back(*m.instantiate<geometry_msgs::TransformStamped>());
        }
    }
}

int main(int argc, char **argv) {
    //Parse Arguments.
    if (argc < 2) {
        ROS_ERROR("Please specify: 1) input bag file and optionally: 2) batch size (default = 10).");
        return -1;
    }

    int batch_size = 10; //default
    if (argc > 2) {
        batch_size = atoi(argv[2]);
        if (batch_size < 1) {
            ROS_ERROR("Invalid batch size argument - terminating...");
            return -1;
        }
    }

    //Initialize ROS node.
    ros::init(argc, argv, "rosbag_to_neural_net");
    ros::NodeHandle n;

    //Prepare rosbag.
    string bag_file = argv[1];
    //Read input Rosbag.
    cout << "Reading input Rosbag..." << endl;
    rosbag::Bag bag_in;
    bag_in.open(bag_file, rosbag::bagmode::Read);

    //Generate output Rosbag.
    rosbag::Bag bag_out;
    cout << "Generating output Rosbag..." << endl;
    path p(bag_file);
    bag_out.open(p.remove_filename().string() + "/output_rosbag.bag", rosbag::bagmode::Write);

    //Specify topics to query.
    vector <string> topics;
    topics.push_back(string("Stereo_Left/Omni_B/rgb_cloud"));
    topics.push_back(string("Stereo_Left/Omni_F/rgb_cloud"));
    topics.push_back(string("Stereo_Left/Omni_L/rgb_cloud"));
    topics.push_back(string("Stereo_Left/Omni_R/rgb_cloud"));
    topics.push_back(string("transform_imu"));

    rosbag::View view(bag_in, rosbag::TopicQuery(topics));

    //Do the actual transformation. (main part)
    transformRosbagToPCL(view, bag_out, batch_size, true);  //Global cloud.
    transformRosbagToPCL(view, bag_out, batch_size, false); //Local clouds.

    cout << "Closing bags..." << endl;
    bag_in.close();
    bag_out.close();
    cout << "Done!" << endl;
    return 0;
}